﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WPFDatePickerSelectedDate
{
    /// <summary>
    /// MainWindow.xaml の相互作用ロジック
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        /**
         * @brief 前日ボタンが押下されたときに呼び出される。
         * 
         * @param [in] sender 前日ボタン
         * @param [in] e イベント
         */
        private void ButtonPrev_Click(object sender, RoutedEventArgs e)
        {
            // 選択された日付を変更します。
            ChangeSelecteDate(-1);
        }

        /**
         * @brief 次日ボタンが押下されたときに呼び出される。
         * 
         * @param [in] sender 次日ボタン
         * @param [in] e イベント
         */
        private void ButtonNext_Click(object sender, RoutedEventArgs e)
        {
            // 選択された日付を変更します。
            ChangeSelecteDate(1);
        }

        /**
         * @brief 選択された日付を変更します。
         * 
         * @param [in] offset 日付をずらすオフセット(前日、次日ボタンが押下された時に指定されます)
         */
        private void ChangeSelecteDate(int offset = 0)
        {
            // 選択された日付を取得します。
            var selectedDate = datePicker.SelectedDate;
            var dateTime = selectedDate ?? DateTime.Now;

            // オフセットが指定されている場合、オフセット分日付をずらします。
            dateTime = dateTime.AddDays(offset);

            // DatePickerの選択日付と表示日付を更新します。
            datePicker.SelectedDate = dateTime;
        }
    }
}
